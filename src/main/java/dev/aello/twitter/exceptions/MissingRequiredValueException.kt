package dev.aello.twitter.exceptions

class MissingRequiredValueException(message: String) : Exception(message)