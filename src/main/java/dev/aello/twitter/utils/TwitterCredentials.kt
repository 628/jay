package dev.aello.twitter.utils

data class TwitterCredentials(val accessToken: String,
                              val accessTokenSecret: String,
                              val consumerApiKey: String,
                              val consumerApiKeySecret: String)