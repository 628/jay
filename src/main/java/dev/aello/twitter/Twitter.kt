package dev.aello.twitter

import com.google.api.client.auth.oauth.OAuthHmacSigner
import com.google.api.client.auth.oauth.OAuthParameters
import com.google.api.client.http.GenericUrl
import dev.aello.twitter.utils.TwitterCredentials
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

class Twitter(private val credentials: TwitterCredentials) {
    private val apiUrl = "https://api.twitter.com/1.1/"

    fun updateStatus(message: String) {
        val url = URL(apiUrl + "statuses/update.json")
        val hmacSigner = OAuthHmacSigner().apply {
            tokenSharedSecret = credentials.accessTokenSecret
            clientSharedSecret = credentials.consumerApiKeySecret
        }

        val oauthParams = OAuthParameters().apply {
            status = message
            consumerKey = credentials.consumerApiKey
            token = credentials.accessToken
            version = "1.0"
            signer = hmacSigner

            computeNonce()
            computeTimestamp()
            computeSignature("POST", GenericUrl(url))
        }

        val connection = (url.openConnection() as HttpURLConnection).apply {
            requestMethod = "POST"
            setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
            setRequestProperty("authorization", oauthParams.authorizationHeader)
            useCaches = false
            doOutput = true
        }

        connection.outputStream.use {
            it.write("status=${urlEncode(message)}".toByteArray())
        }
        connection.inputStream.close()
    }

    private fun urlEncode(str: String): String {
        return URLEncoder.encode(str, StandardCharsets.UTF_8.toString())
    }
}