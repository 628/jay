# Jay
Twitter API wrapper for Java/Kotlin.
## Usage
Send a Tweet:

```java
public static void main(String[] args) {
    TwitterCredentials creds = new TwitterCredentials("accessToken",
                                        "accessTokenSecret",
                                        "consumerApiKey",
                                        "consumerApiKeySecret");
    
    Twitter twit = new Twitter(creds);
    twit.updateStatus("My cool status!");
}
```

I plan to add additional functionality in the future.
## Installation
### Maven
Add the repository:
```xml
<repositories>
	<repository>
	    <id>jitpack.io</id>
	    <url>https://jitpack.io</url>
	</repository>
</repositories>
```
Add the dependency:
```xml
<dependency>
    <groupId>com.gitlab.628</groupId>
    <artifactId>jay</artifactId>
    <version>v1.0.0</version>
</dependency>
```
### Gradle
Add the repository:
```groovy
repositories {
	maven { url 'https://jitpack.io' }
}
```
Add the dependency:
```groovy
dependencies {
        implementation 'com.gitlab.628:jay:v1.0.0'
}
```